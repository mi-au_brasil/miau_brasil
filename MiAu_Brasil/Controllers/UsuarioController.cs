﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MI_AU_Brasil.Models;

namespace MI_AU_Brasil.Controllers
{
    public class UsuarioController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        #region Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Create(Usuario user)
        {
            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Details
        public IActionResult Details()
        {
            return View();
        }
        #endregion

        #region Edit

        public IActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Edit(Usuario user)
        {
            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Delete
        public IActionResult Delete()
        {
            return View();
        }

        public IActionResult Delete(Usuario user)
        {
            return RedirectToAction(nameof(Index));
        }

        #endregion
    }
}
