﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI_AU_Brasil.Models
{
    public class Usuario
    {
        public int cpf { get; set; }
        public int rg { get; set; }
        public string genero { get; set; }
        public string nome { get; set; }
        public int telefoneFixo { get; set; }
        public int telefoneCell { get; set; }
        public string email { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
    }
}
