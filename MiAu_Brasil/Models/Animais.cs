﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MI_AU_Brasil.Models
{
    public class Animais
    {
        public string nome { get; set; }
        public string tipo { get; set; }
        public string genero { get; set; }
        public string deficiencia { get; set; }
        public bool vacinado { get; set; }
        public bool catrado { get; set; }
        public int tamanho { get; set; }
        public int peso { get; set; }
        public string temperamento { get; set; }
        public string observacao { get; set; }
        public DateTime dataResgate { get; set; }
        public int idade { get; set; }
        public string foto { get; set; }
        public string porte { get; set; }
    }
}
